FROM docker.io/library/debian:stable-slim

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -q update && \
    apt-get install -y --no-install-recommends golang jsonnet git ca-certificates curl apt-transport-https gnupg && \
    curl -sf https://baltocdn.com/helm/signing.asc | gpg --dearmor > /usr/share/keyrings/helm.gpg && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" > /etc/apt/sources.list.d/helm-stable-debian.list && \
    apt-get -q update && \
    apt-get install -y --no-install-recommends helm && \
    go install github.com/grafana/tanka/cmd/tk@latest && \
    mv /root/go/bin/tk /usr/bin/tk

